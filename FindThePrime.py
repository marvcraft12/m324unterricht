max_num = int(input("Bitte geben Sie die maximale Zahl ein: "))
# Primzahlen finden
print("Die Primzahlen bis zu der eingegebenen Zahl sind: ")
for number in range(2, max_num + 1):
    is_prime = True
    for i in range(2, int(number ** 0.5) + 1):
        if number % i == 0:
            is_prime = False
            break
    if is_prime:
        print(number)
